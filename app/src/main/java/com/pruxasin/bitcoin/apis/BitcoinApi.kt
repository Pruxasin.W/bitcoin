package com.pruxasin.bitcoin.apis

import android.content.Context
import android.util.Log
import com.pruxasin.bitcoin.ApiClient
import com.pruxasin.bitcoin.ApiService
import com.pruxasin.bitcoin.interfaces.BitcoinAdapterInterface
import com.pruxasin.bitcoin.items.BitcoinItem
import com.pruxasin.bitcoin.pojos.BitcoinPojo
import retrofit2.Call
import retrofit2.Callback
import retrofit2.Response

class BitcoinApi {
    private var mContext: Context
    private lateinit var mBitcoinAdapterInterface: BitcoinAdapterInterface

    constructor(context: Context) {
        this.mContext = context
    }

    fun registerBitcoinAdapterInterface(bitcoinAdapterInterface: BitcoinAdapterInterface) {
        this.mBitcoinAdapterInterface = bitcoinAdapterInterface
    }

    fun doRequest(timePeriod: String) {
        val apiService: ApiService = ApiClient().getRetrofit()!!.create(ApiService::class.java)

        val apiCall: Call<BitcoinPojo>?
        apiCall = if (timePeriod.isEmpty()) {
            apiService.requestBitcoinList()
        } else {
            apiService.requestBitcoinListWithTimePeriod(timePeriod)
        }
        apiCall.enqueue(object : Callback<BitcoinPojo> {
            override fun onFailure(call: Call<BitcoinPojo>, t: Throwable) {
                Log.e("_Pruxasin", "bitcoin api call on failed: $t")
            }

            override fun onResponse(call: Call<BitcoinPojo>, response: Response<BitcoinPojo>) {
                if (response.code() != 200) {
                    Log.e("_Pruxasin", "response != 200")
                } else {
                    var bitcoinItemList: ArrayList<BitcoinItem> = ArrayList()

                    val bitcoinPojo: BitcoinPojo? = response.body()
                    val data: BitcoinPojo.Data? = bitcoinPojo!!.data
                    val coins: List<BitcoinPojo.Data.Coin>? = data!!.coins

                    for (coin in coins!!) {
                        val item = BitcoinItem()
                        item.id = coin.id
                        item.iconUrl = coin.iconUrl
                        item.name = coin.name
                        item.description = coin.description

                        bitcoinItemList.add(item)
                    }

                    mBitcoinAdapterInterface.setBitcoinListToRecyclerView(bitcoinItemList)
                }
            }
        })
    }
}