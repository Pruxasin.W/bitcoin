package com.pruxasin.bitcoin.activities

import android.content.Context
import android.graphics.Color
import android.os.Bundle
import android.widget.CompoundButton
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.pruxasin.bitcoin.R
import com.pruxasin.bitcoin.adapters.BitcoinAdapter
import com.pruxasin.bitcoin.apis.BitcoinApi
import com.pruxasin.bitcoin.interfaces.BitcoinAdapterInterface
import com.pruxasin.bitcoin.items.BitcoinItem
import kotlinx.android.synthetic.main.activity_main.*

class MainActivity : AppCompatActivity(), BitcoinAdapterInterface {

    private lateinit var mContext: Context

    private var mBitcoinItemList: ArrayList<BitcoinItem> = ArrayList()
    private lateinit var mBitcoinAdapter: BitcoinAdapter
    private lateinit var mBitcoinApi: BitcoinApi
    private lateinit var mLinearLayoutManager: LinearLayoutManager
    private lateinit var mCurrentTimePeriod: String
    private lateinit var mBitcoinItemDecoration: RecyclerView.ItemDecoration
    private var mLimit: Int = 10

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)

        mContext = this
        mCurrentTimePeriod = ""
        mLimit = 10

        fetchBitcoinData(mCurrentTimePeriod)

        setSwipeRefreshLayout()

        setOnCheckedChangListenerAllRadioButtonPeriods()
    }

    override fun setBitcoinListToRecyclerView(bitcoinItemList: ArrayList<BitcoinItem>) {
        if (activity_main_recycler_view_bitcoin_list.itemDecorationCount != 0) {
            activity_main_recycler_view_bitcoin_list.removeItemDecoration(mBitcoinItemDecoration)
        }

        this.mBitcoinItemList = bitcoinItemList

        mLinearLayoutManager = LinearLayoutManager(mContext, LinearLayoutManager.VERTICAL, false)
        activity_main_recycler_view_bitcoin_list.layoutManager = mLinearLayoutManager
        mBitcoinAdapter = BitcoinAdapter(mContext, mBitcoinItemList)
        activity_main_recycler_view_bitcoin_list.adapter = mBitcoinAdapter

        mBitcoinItemDecoration = DividerItemDecoration(mContext, DividerItemDecoration.VERTICAL)
        activity_main_recycler_view_bitcoin_list.addItemDecoration(mBitcoinItemDecoration)

//        setBitcoinRecyclerViewListener()

        activity_main_swipe_refresh_layout_bitcoin_list.isRefreshing = false
    }

    override fun updateRecyclerview(bitcoinItemList: ArrayList<BitcoinItem>) {
        this.mBitcoinItemList.addAll(bitcoinItemList)

        if (activity_main_recycler_view_bitcoin_list.itemDecorationCount != 0) {
            activity_main_recycler_view_bitcoin_list.removeItemDecoration(mBitcoinItemDecoration)
        }

        mBitcoinAdapter.notifyDataSetChanged()
    }

/*    private fun setBitcoinRecyclerViewListener() {
        activity_main_recycler_view_bitcoin_list.addOnScrollListener(object :
            RecyclerView.OnScrollListener() {
            override fun onScrollStateChanged(recyclerView: RecyclerView, newState: Int) {
                super.onScrollStateChanged(recyclerView, newState)
                val itemCount = activity_main_recycler_view_bitcoin_list.layoutManager!!.itemCount
                if (itemCount == getLastVisibleItemPosition + 1) {
                    mLimit += 10
                    fetchBitcoinDataWithLimit(mCurrentTimePeriod)
                    activity_main_swipe_refresh_layout_bitcoin_list.isRefreshing = false
                }
            }
        })
    }*/

/*    private fun fetchBitcoinDataWithLimit(timePeriod: String) {
        mBitcoinApi = BitcoinApi(mContext)
        mBitcoinApi.registerBitcoinAdapterInterface(this)
        mBitcoinApi.doRequest(timePeriod, mLimit)
    }*/

    private val getLastVisibleItemPosition: Int
        get() = mLinearLayoutManager.findLastVisibleItemPosition()


    private fun fetchBitcoinData(timePeriod: String) {
        activity_main_swipe_refresh_layout_bitcoin_list.isRefreshing = true

        mBitcoinApi = BitcoinApi(mContext)
        mBitcoinApi.registerBitcoinAdapterInterface(this)
        mBitcoinApi.doRequest(timePeriod)
    }

    private fun setSwipeRefreshLayout() {
        activity_main_swipe_refresh_layout_bitcoin_list.setProgressBackgroundColorSchemeColor(
            ContextCompat.getColor(mContext, R.color.colorPrimary)
        )
        activity_main_swipe_refresh_layout_bitcoin_list.setColorSchemeColors(Color.WHITE)
        activity_main_swipe_refresh_layout_bitcoin_list.setOnRefreshListener {
            this.mBitcoinItemList.clear()
            activity_main_recycler_view_bitcoin_list.adapter!!.notifyDataSetChanged()

            fetchBitcoinData(mCurrentTimePeriod)
        }
    }

    private fun setOnCheckedChangListenerAllRadioButtonPeriods() {
        activity_main_radio_button_period_24h.setOnCheckedChangeListener(
            period24hRadioButtonOnCheckedChangeListener
        )
        activity_main_radio_button_period_7d.setOnCheckedChangeListener(
            period7dRadioButtonOnCheckedChangeListener
        )
        activity_main_radio_button_period_30d.setOnCheckedChangeListener(
            period30dRadioButtonOnCheckedChangeListener
        )
    }

    private val period24hRadioButtonOnCheckedChangeListener =
        CompoundButton.OnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                view.setTextColor(ContextCompat.getColor(mContext, android.R.color.white))

                mCurrentTimePeriod = getString(R.string.period_24h)
                fetchBitcoinData(mCurrentTimePeriod)
            } else {
                view.setTextColor(ContextCompat.getColor(mContext, android.R.color.darker_gray))
            }
        }

    private val period7dRadioButtonOnCheckedChangeListener =
        CompoundButton.OnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                view.setTextColor(ContextCompat.getColor(mContext, android.R.color.white))

                mCurrentTimePeriod = getString(R.string.period_7d)
                fetchBitcoinData(mCurrentTimePeriod)
            } else {
                view.setTextColor(ContextCompat.getColor(mContext, android.R.color.darker_gray))
            }
        }

    private val period30dRadioButtonOnCheckedChangeListener =
        CompoundButton.OnCheckedChangeListener { view, isChecked ->
            if (isChecked) {
                view.setTextColor(ContextCompat.getColor(mContext, android.R.color.white))

                mCurrentTimePeriod = getString(R.string.period_30d)
                fetchBitcoinData(mCurrentTimePeriod)
            } else {
                view.setTextColor(ContextCompat.getColor(mContext, android.R.color.darker_gray))
            }
        }

    companion object {
        private const val RECYCLER_VIEW_STATE_KEY = "RECYCLER_VIEW_STATE_KEY"
    }
}
