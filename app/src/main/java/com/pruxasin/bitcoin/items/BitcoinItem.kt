package com.pruxasin.bitcoin.items

import android.os.Parcel
import android.os.Parcelable

class BitcoinItem() : Parcelable {
    var id: Int? = 0
    var iconUrl: String? = ""
    var name: String? = ""
    var description: String? = ""

    constructor(parcel: Parcel) : this() {
        id = parcel.readValue(Int::class.java.classLoader) as? Int
        iconUrl = parcel.readString()
        name = parcel.readString()
        description = parcel.readString()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeValue(id)
        parcel.writeString(iconUrl)
        parcel.writeString(name)
        parcel.writeString(description)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<BitcoinItem> {
        override fun createFromParcel(parcel: Parcel): BitcoinItem {
            return BitcoinItem(parcel)
        }

        override fun newArray(size: Int): Array<BitcoinItem?> {
            return arrayOfNulls(size)
        }
    }
}