package com.pruxasin.bitcoin

import com.pruxasin.bitcoin.pojos.BitcoinPojo
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiService {
    @GET("v1/public/coins")
    fun requestBitcoinList(): Call<BitcoinPojo>

    @GET("v1/public/coins?")
    fun requestBitcoinListWithTimePeriod(@Query("timePeriod") timePeriod: String): Call<BitcoinPojo>
}