package com.pruxasin.bitcoin.pojos

import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName

class BitcoinPojo {
    @SerializedName("status")
    @Expose
    var status: String? = null
    @SerializedName("data")
    @Expose
    var data: Data? = null

    class Data {
        @SerializedName("coins")
        @Expose
        var coins: List<Coin>? = null

        class Coin {
            @SerializedName("id")
            @Expose
            var id: Int? = null
            @SerializedName("name")
            @Expose
            var name: String? = null
            @SerializedName("description")
            @Expose
            var description: String? = null
            @SerializedName("iconUrl")
            @Expose
            var iconUrl: String? = null
        }
    }
}
