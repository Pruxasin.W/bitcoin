package com.pruxasin.bitcoin.adapters

import android.content.Context
import android.graphics.drawable.PictureDrawable
import android.net.Uri
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import androidx.recyclerview.widget.RecyclerView
import com.bumptech.glide.load.resource.drawable.DrawableTransitionOptions.withCrossFade
import com.pruxasin.bitcoin.R
import com.pruxasin.bitcoin.items.BitcoinItem
import com.pruxasin.bitcoin.svgtool.GlideApp
import com.pruxasin.bitcoin.svgtool.SvgSoftwareLayerSetter
import kotlinx.android.synthetic.main.list_item_bitcoin.view.*


class BitcoinAdapter : RecyclerView.Adapter<BitcoinAdapter.BitcoinViewHolder> {

    private var mContext: Context
    private var mBitcoinItemList: List<BitcoinItem>

    constructor(context: Context, bitcoinItemList: List<BitcoinItem>) {
        mContext = context
        mBitcoinItemList = bitcoinItemList
    }

    class BitcoinViewHolder(private val view: View, private val context: Context) :
        RecyclerView.ViewHolder(view) {

        fun bindView(bitcoinItem: BitcoinItem) {
            val requestBuilder = GlideApp.with(context)
                .`as`(PictureDrawable::class.java)
                .transition(withCrossFade())
                .listener(SvgSoftwareLayerSetter())

            val uri = Uri.parse(bitcoinItem.iconUrl)
            requestBuilder.load(uri).into(view.list_item_bitcoin_image_view_icon)

            view.list_item_bitcoin_name_text_view.text = bitcoinItem.name
            view.list_item_bitcoin_description_text_view.text = bitcoinItem.description
        }
    }

    override fun onCreateViewHolder(
        parent: ViewGroup,
        viewType: Int
    ): BitcoinAdapter.BitcoinViewHolder {
        val inflatedView = parent.inflate(R.layout.list_item_bitcoin, false)
        return BitcoinViewHolder(inflatedView, mContext)
    }

    override fun getItemCount(): Int = mBitcoinItemList.size

    override fun onBindViewHolder(holder: BitcoinViewHolder, position: Int) {
        val bitcoinItem: BitcoinItem = mBitcoinItemList.get(position)
        holder.bindView(bitcoinItem)

    }

    private fun ViewGroup.inflate(@LayoutRes layoutRes: Int, attachToRoot: Boolean = false): View {
        return LayoutInflater.from(context).inflate(layoutRes, this, attachToRoot)
    }
}