package com.pruxasin.bitcoin.interfaces

import com.pruxasin.bitcoin.items.BitcoinItem

interface BitcoinAdapterInterface {
    fun setBitcoinListToRecyclerView(bitcoinItemList: ArrayList<BitcoinItem>)
    fun updateRecyclerview(bitcoinItemList: ArrayList<BitcoinItem>)
}