package com.pruxasin.bitcoin

import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.gson.GsonConverterFactory

class ApiClient {
    private var mRetrofit: Retrofit? = null
    private val mBaseUrl: String = "https://api.coinranking.com/"

    fun getRetrofit(): Retrofit? {
        val logging = HttpLoggingInterceptor()
        logging.apply {
            level =
                if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        }

        val client: OkHttpClient = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()

        mRetrofit = Retrofit.Builder()
            .baseUrl(mBaseUrl)
            .client(client)
            .addConverterFactory(GsonConverterFactory.create())
            .build()

        return mRetrofit
    }
}